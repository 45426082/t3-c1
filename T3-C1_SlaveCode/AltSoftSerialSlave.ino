#include <AltSoftSerial.h>
AltSoftSerial BTserial; 

char in = ' ';
char out = ' ';
boolean NL = true;
int LED = 4;
int buttonState = 0;


/**
 * AltSoftSerial requires
 * Pin D8 for TX
 * Pin D9 for RX
 * ADDR-1: 90E202915648 - RED one!
 * ADDR-2: 90E2029FE20D - BLUE one!
 * connect this to the blue one
 */
void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);
  Serial.print("Sketch:   ");   Serial.println(__FILE__);
  Serial.print("Uploaded: ");   Serial.println(__DATE__);
  Serial.println(" ");
 
  BTserial.begin(9600);  
  Serial.println("BTserial started at 9600");
  Serial.println("Bluetooth Connection State: " + bluetoothCheckState());
  Serial.println(" ");
  bluetoothConnect();
  Serial.println("Bluetooth Connection State: " + bluetoothCheckState());
  Serial.println(" ");
  pinMode(LED,OUTPUT);
}

void loop() {
  // put your main code here, to run repeatedly:
  buttonState = BTserial.read();
  Serial.println(buttonState);
  if(buttonState == 49){//49 is ascii for 1 and 48 is ascii for 0
    digitalWrite(LED,HIGH);
  } else {
    digitalWrite(LED, LOW);
  }

}

void bluetoothConnect(){
  BTserial.print("AT+IMME0" );
  delay(1000);
  BTserial.print("AT+ROLE0" );
  delay(1000);
  BTserial.print("AT+CON90E202915648");
  delay(1000);
}

String bluetoothCheckState() {
  String response = "";
  BTserial.print("AT+STATE?");
  response = BTserial.read();
  return response;
}
