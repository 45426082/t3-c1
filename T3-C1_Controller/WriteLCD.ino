void writeMsg(char c){//switch case to print msg out to lcd screen
  //lcd.clear();
  /*
   * Switch cases to display acknowledgments
   * that have been sent back, and also the status
   * messages that are being sent back;
   * this will be displayed on the LCD screen
   * this will also halt the keep sending command
   */
  switch(c){
    case 's':
      STARTKEEPSENDING = 0;
      lcd.setCursor(1,1);
      lcd.print("GO!       ");
      //buttonStartToggle = 0;
      //delay(2000);
      break;
    case 'h':
      STARTKEEPSENDING = 0;
      lcd.setCursor(1,1);
      lcd.print("STOP!     ");
      //buttonStartToggle = 0;
      //delay(2000);
      break;
    case 'l':
      DIRKEEPSENDING = 0;
      lcd.setCursor(6,1);
      lcd.print("     EAST!");
      //buttonDirToggle = 0;
      break;
    case 'r':
      DIRKEEPSENDING = 0;
      lcd.setCursor(6,1);
      lcd.print("     WEST!");
      //buttonDirToggle = 0;
      break;
    case 'o':
      DOORKEEPSENDING = 0;
      lcd.setCursor(1,1);
      lcd.print("     OPEN!   ");
      break;
    case 'c':
      DOORKEEPSENDING = 0;
      lcd.setCursor(1,1);
      lcd.print("   CLOSE!    ");
      break;
    case '!':
      ESKEEPSENDING = -1;
      lcd.setCursor(1,1);
      lcd.print("  EMERGENCY!  ");
      setup();
      break;
    case 'x':
      lcd.setCursor(1,0);
      lcd.print("   Starting    ");
      break;
    case 'u':
      lcd.setCursor(1,0);
      lcd.print("   Stopping    ");
      break;
    case 'v':
      lcd.setCursor(1,0);
      lcd.print("  Going East   ");
      break;
    case 'w':
      lcd.setCursor(1,0);
      lcd.print("  Going West   ");
      break;
    case 'z':
      lcd.setCursor(1,0);
      lcd.print("  Emergency   ");
      break;
    default:
      break;
    
  }
}
