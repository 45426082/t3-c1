void bluetoothConnect(){//for initial connection

  BTserial.print("AT+RENEW");//renew module for connection
  delay(1000);

  BTserial.print("AT+IMME1" );//connect immediately

  
  delay(1000);
    
  BTserial.print("AT+ROLE1");//set to the MASTER ROLE
  
  delay(1000);
  //BTserial.print("AT+CON90E202915648");//con to red
  //BTserial.print("AT+CON90E2029FE20D"); //con to blue
  //BTserial.print("AT+CON1862E43E75EE"); //con to red actual
  BTserial.print("AT+CON1862E43E7D9D"); //con to green actual

  delay(1000);
}

String bluetoothCheckState() {//should print out AT+CONN if connected
  String response = "";
  BTserial.print("AT+STATE?");
  response = BTserial.read();
  
  return response;
}
