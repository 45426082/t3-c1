void hold(){//holds the screen at the very start, and after power cycle
  while(true){
    stBounce.update();//update bounce for the button

    int start = stBounce.read();//set bounce variable to an int for easier tracability

    if(start == HIGH){
      digitalWrite(STARTLED, HIGH);
    } else {
      digitalWrite(STARTLED, LOW);
    }
    
    char q = Serial.read();
    if(q == 's' || start == LOW){//if start button pressed, exit this mode and enter full loop
      //BTserial.clear();
      BTserial.print('s');
      Serial.print('s');
      digitalWrite(STARTLED, LOW);
      break;
    }
  }
}
