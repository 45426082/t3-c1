#include <AltSoftSerial.h>
#include <LiquidCrystal_I2C.h>
#include <Bounce2.h>
AltSoftSerial BTserial; 

Bounce stBounce = Bounce();
Bounce dirBounce = Bounce();
Bounce doorBounce = Bounce();
Bounce esBounce = Bounce();

Bounce immeBounce = Bounce();
Bounce roleBounce = Bounce();
Bounce connBounce = Bounce();

int lastStart;
int lastDir;

int startSent;
int dirSent;
 
char c=' ';
char rec = ' ';
boolean NL = true;
char str[34];
int i = 0;
bool first = true;

int arr[4];

int STARTBUTTON = 7;//green
int DIRBUTTON = 6;//blue
int DOORBUTTON = 5;//yellow
int ESBUTTON = 4;//red

int STARTLED = 13;//green
int DIRLED = 12;//blue
int DOORLED = 11;//yellow
int ESLED = 10;//red

int ESKEEPSENDING;
int STARTKEEPSENDING;
int DIRKEEPSENDING;
int DOORKEEPSENDING;

//int buttonStartState = 0;
int buttonStartToggle;

//int buttonDirState = 0;
int buttonDirToggle;

//int buttonDoorState = 0;
int buttonDoorToggle;

//int buttonEmergencyState = 0;
int buttonEmergencyToggle;

LiquidCrystal_I2C lcd(0x27, 20, 4);

/*
 * For the actual assignment:
 * ADDR-1: 1862E43E75EE - RED one!
 * ADDR-2: 1862E43E7D9D - GREEN one!
 */

void setup() {
  // put your setup code here, to run once:
  lcd.init();
  lcd.noBacklight();
  Serial.begin(9600);
  pinMode(STARTBUTTON,INPUT_PULLUP);
  pinMode(DIRBUTTON, INPUT_PULLUP);
  pinMode(DOORBUTTON, INPUT_PULLUP);
  pinMode(ESBUTTON, INPUT_PULLUP);

  pinMode(STARTLED, OUTPUT);
  pinMode(DIRLED, OUTPUT);
  pinMode(DOORLED, OUTPUT);
  pinMode(ESLED, OUTPUT);

  stBounce.attach(STARTBUTTON);
  stBounce.interval(5);
  dirBounce.attach(DIRBUTTON);
  dirBounce.interval(5);
  doorBounce.attach(DOORBUTTON);
  doorBounce.interval(5);
  esBounce.attach(ESBUTTON);
  esBounce.interval(5);

  immeBounce.interval(5);
  roleBounce.interval(10);
  connBounce.interval(15);
  
  Serial.print("Sketch:   ");   Serial.println(__FILE__);
  Serial.print("Uploaded: ");   Serial.println(__DATE__);
  Serial.println(" ");
 
  BTserial.begin(9600);
  Serial.println("BTserial started at 9600");
  Serial.println("Bluetooth Connection State: " + bluetoothCheckState());
  Serial.println(" ");
  bluetoothConnect();
  Serial.println("Bluetooth Connection State: " + bluetoothCheckState());
  Serial.println(" ");

  ESKEEPSENDING = 0;
  STARTKEEPSENDING = 0;
  DIRKEEPSENDING = 0;
  DOORKEEPSENDING = 0;

  buttonStartToggle = 0;
  buttonDirToggle = 0;
  buttonDoorToggle = 0;
  buttonEmergencyToggle = -1;

  lastStart = HIGH;
  lastDir = HIGH;

  startSent = 0;
  dirSent = 0;

  lcd.init();
  lcd.init();
  lcd.backlight();
  lcd.setCursor(1,0);
  lcd.print("Welcome to T3:");
  lcd.setCursor(1,1);
  hold();
}


void loop() {
  // put your main code here, to run repeatedly:
  //update the bounce variables, for the button input
  stBounce.update();
  dirBounce.update();
  doorBounce.update();
  esBounce.update();


  //reads bounce variable into an int for easer tracability
  int stB = stBounce.read();
  int dirB = dirBounce.read();
  int doorB = doorBounce.read();
  int esB = esBounce.read();

  /*
   * emergency stop button,
   * does not have to toggle
   */
  if(esB == LOW || ESKEEPSENDING == 1)
  {
    ESKEEPSENDING = 1;
    if(buttonEmergencyToggle == -1 && esB == LOW)//if not stopped currently and buttotn is pressed
    {
      digitalWrite(ESLED, HIGH);
      buttonEmergencyToggle = 0;
      BTserial.println("!");
      buttonStartToggle = -1;
      buttonDirToggle = -1;
      buttonDoorToggle = -1;
      
      STARTKEEPSENDING = 0;
      DIRKEEPSENDING = 0;
      DOORKEEPSENDING = 0;
    }
  } else if(esB == HIGH){//if not pressed, turn off the LCD
    digitalWrite(ESLED, LOW);
  }

  /*
   * starting and stopping button commands,
   * has to have toggling capabilities
   */
  if((stB == LOW || STARTKEEPSENDING == 1) && buttonStartToggle != -1)
  {
    
    if(buttonStartToggle == 1 && stB == LOW && STARTKEEPSENDING == 0 && lastStart != LOW)
    {//if button is currently being pressed, if the toggle has to change to go to stop
      digitalWrite(STARTLED, HIGH);
      buttonStartToggle = 2;
      Serial.println("Sending h");
      BTserial.println("h");
      lastStart = LOW;
      
    }
    else if (stB == LOW && STARTKEEPSENDING == 0 && lastStart != LOW)
    {//if button is currently being pressed, if the toggle has to change to start
      digitalWrite(STARTLED,HIGH);
      buttonStartToggle = 1;
      Serial.println("Sending s");
      BTserial.println("s");
      lastStart = LOW;
     }
     else if(buttonStartToggle == 1 && stB == HIGH && startSent > 500)
     {//if the button is not being pressed, keep sending start
      BTserial.println("s");
      Serial.println("Sending s");
      startSent = 0;
      lastStart = HIGH;
     } else if (buttonStartToggle == 2 && stB == HIGH && startSent > 500)
     {//if the button is not being pressed, keep sending stop
      BTserial.println("h");
      Serial.println("Sending h");
      startSent = 0;
      lastStart = HIGH;
     } 

     if(stB == HIGH){//if button is not being pressed, dont turn on LED
      digitalWrite(STARTLED,LOW);
     }

      STARTKEEPSENDING = 1;
      startSent++;//to only re-send commands once every 500 loops
  }


  /*
   * for the button pressed for the directions
   */
  if((dirB == LOW || DIRKEEPSENDING == 1) && buttonDirToggle != -1)
  {
    if(buttonDirToggle == 3 && dirB == LOW && DIRKEEPSENDING == 0 && lastDir != LOW)
    {//if button is currently being pressed, if the toggle has to change to go to west
      digitalWrite(DIRLED,HIGH);
      buttonDirToggle = 4;
      BTserial.println("l");
      lastDir = LOW;
      
      }
    else if (dirB == LOW && DIRKEEPSENDING == 0 && lastDir != LOW)
    {//if button is currently being pressed, if the toggle has to change to go to east
      digitalWrite(DIRLED,HIGH);
      buttonDirToggle = 3;
      BTserial.println("r");
      lastDir = LOW;
      
    }
    else if(buttonDirToggle == 4 && dirB == HIGH && dirSent > 500)
    {//if the button is not being pressed, keep sending west
      BTserial.println("l");
      dirSent = 0;
      lastDir = HIGH;
    } else if(buttonDirToggle == 3 && dirB ==HIGH && dirSent > 500) 
    {//if the button is not being pressed, keep sending west
      BTserial.println("r");
      dirSent = 0;
      lastDir = HIGH;
    }

      if(dirB == HIGH){//if button is not being pressed, dont turn on LED
        digitalWrite(DIRLED, LOW);
      }

      dirSent++;//to only re-send commands once every 500 loops
      DIRKEEPSENDING = 1;
}

    /*
     *  Buttons were never implemented, so simply display that it is an invalid button
     */
    if(doorB == LOW){
      digitalWrite(DOORLED, HIGH);
      lcd.setCursor(1,1);
      lcd.print("Invalid Button!      ");
    } else {
      digitalWrite(DOORLED, LOW);
    }
      // Read from the Bluetooth module and send to the Arduino Serial Monitor
    if (BTserial.available())
    {
       rec = BTserial.read();
       Serial.write(rec);
        
       writeMsg(rec);
       
        
    }
 
    //for debugging purposes 
    // Read from the Serial Monitor and send to the Bluetooth module
    if (Serial.available())
    {
        c = Serial.read();
 
        // do not send line end characters to the HM-10
        if (c!=10 & c!=13 ) 
        {  
             BTserial.write(c);
        }
 
        // Echo the user input to the main window. 
        // If there is a new line print the ">" character.
        if (NL) { Serial.print("\r\n>");  NL = false; }
        Serial.write(c);
        if (c==10) { NL = true; }
    }
}
